# Tic Tac Toe MCTS Policy/Value Generator
Generates policies and state values using Monte Carlo Tree Search on 3x3 Tic-Tac-Toe game.

## Usage
After building `xox-pq-generator` using CMake, you can run it as a single process with command

    USAGE: xox-pq-generator OUTPUT_NAME GAME_COUNT

which will run generator to play `GAME_COUNT` games and save output data in `OUTPUT_NAME_pol.dat` and `OUTPUT_NAME_val.dat` files.

### Multi-process
If you are planning to make it play thousands of games, it would be better to run it multi-processed. In `utils` folder you can find a Python script to help with this issue.

#### Usage

    usage: generate_data.py [-h] executable_path output_name process_count game_count

Script takes 4 arguments. 

   * executable_path => path to `xox-pq-generator` binary
   * output_name => same as generator
   * process_count => how many processes should share the games. please set this value to your processor's cores at maximum.
   * game_count => how many games should be played
   
## Output Formats
### Policies

    1,1,1,1,1,1,1,1,1, # 9 ones, indicating X to play (zeros indicate O to play)
    0,1,0,1,1,0,0,0,1, # X positions on board
    1,0,1,0,0,0,1,0,0, # O positions on board
    # and action probabilities
    0.000000,0.000000,0.000000,0.000000,0.000000,0.102574,0.000000,0.103232,0.000000

### State Values
   
    1,1,1,1,1,1,1,1,1, # 9 ones, indication X to play (zeros indicate O to play)
    0,0,0,0,0,1,0,0,0, # X positions on board
    0,0,0,0,0,0,1,0,0, # O positions on board
    # state value, range of values changing by simulation time
    12.290000
   
## Contributing
Anyone can send pull requests for any kind of improvements.

## License
See GNU Public License v3