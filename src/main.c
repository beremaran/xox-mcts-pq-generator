/*
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <sys/stat.h>

#define BOARD_SIZE  9

#define PLAYER1     'X'
#define PLAYER2     'O'
#define EMPTY       '.'
#define ONGOING     'K'
#define DRAW        'D'

static unsigned GAME_COUNT = 5000;
FILE *f_b_val;
FILE *f_b_pol;

// ===== TYPE DEFINITIONS =====
typedef char player_t;
typedef player_t *board_t;

// ===== STRUCTURES
typedef struct node Node;
struct node {
    Node *parent;
    long double value;
    uint64_t visits;
    short child_count;
    Node **children;
    board_t board;
    player_t player;
};

typedef struct player Player;
struct player {
    player_t symbol;
    unsigned score;
};

static const int WIN_STATES[8][3] = {
        // horizontal
        {0, 1, 2},
        {3, 4, 5},
        {6, 7, 8},
        // vertical
        {0, 3, 6},
        {1, 4, 7},
        {2, 5, 8},
        // diagonal
        {0, 4, 8},
        {2, 4, 6}
};

// =========== BOARD =================

void board_dump_numeric(FILE *fp, board_t *board) {
    for (int pass = 0; pass < 2; pass++) {
        for (int i = 0; i < BOARD_SIZE; i++) {
            int n = 0;
            switch ((*board)[i]) {
                case PLAYER1:
                    n = (pass == 0) ? 1 : 0;
                    break;
                case PLAYER2:
                    n = (pass == 1) ? 1 : 0;
                    break;
                case EMPTY:
                default:
                    n = 0;
            }
            fprintf(fp, "%d,", n);
        }
    }
}

short board_empty_count(board_t *board) {
    short out = 0;

    for (int i = 0; i < BOARD_SIZE; i++) {
        if ((*board)[i] == EMPTY)
            out++;
    }

    return out;
}

player_t board_status(board_t *board) {
    board_t b = *board;
    for (int i = 0; i < 8; i++) {
        if (b[WIN_STATES[i][0]] == b[WIN_STATES[i][1]]
            && b[WIN_STATES[i][1]] == b[WIN_STATES[i][2]]
            && b[WIN_STATES[i][0]] != EMPTY) {
            return b[WIN_STATES[i][0]];
        }
    }

    if (board_empty_count(board) == 0) {
        return DRAW;
    }

    return ONGOING;
}

board_t board_empty() {
    board_t out = (player_t *) malloc(sizeof(player_t) * BOARD_SIZE);
    for (int i = 0; i < BOARD_SIZE; i++) {
        out[i] = EMPTY;
    }

    return out;
}

int board_diff(board_t b1, board_t b2) {
    for (int i = 0; i < BOARD_SIZE; i++) {
        if (b1[i] != b2[i]) {
            return i;
        }
    }

    return -1;
}

board_t board_copy(board_t *b) {
    board_t out = (board_t) malloc(sizeof(player_t) * BOARD_SIZE);
    memcpy(out, *b, BOARD_SIZE);
    return out;
}

int *board_legal_moves(board_t board) {

    int tc = 0;
    int *out = (int *) malloc(sizeof(int) * BOARD_SIZE);
    for (int i = 0; i < BOARD_SIZE; i++) {
        out[i] = -333;
    }
    for (int i = 0; i < BOARD_SIZE; i++) {
        if (board[i] == EMPTY)
            out[tc++] = i;
    }

    return out;
}

player_t board_next_player(player_t p) {
    return (player_t) ((p == PLAYER1) ? PLAYER2 : PLAYER1);
}

// =========== MCTS =================
long double mcts_ucb(Node *n) {
    long double average_value = n->value / n->visits;
    return average_value
           + sqrtl(
            (log2l(n->parent->visits))
            / (long double) (n->visits >> 2));
}

void mcts_destroy(Node *n) {
    if (n == NULL)
        return;

    if (n->children != NULL) {
        for (int i = 0; i < n->child_count; i++) {
            if (n->children[i] != NULL)
                mcts_destroy(n->children[i]);
        }
        free(n->children);
        n->children = NULL;
    }


    if (n->board != NULL)
        free(n->board);
    n->board = NULL;
    free(n);
}

Node *mcts_best(Node *root) {
    if (root->child_count == 0 || board_status(&(root->board)) != ONGOING)
        return root;

    Node *bn = root->children[0];
    long double bv = mcts_ucb(root->children[0]);
    for (int i = 0; i < root->child_count; i++) {
        long double nv = mcts_ucb(root->children[i]);
        if (nv > bv) {
            bn = root->children[i];
            bv = nv;
        }
    }
    return bn;
}

void mcts_expand(Node *root) {
    int ec = board_empty_count(&(root->board));
    int *legal_moves = board_legal_moves(root->board);

    if (root->children == NULL) {
        root->children = (Node **) malloc(sizeof(Node *) * ec);
        for (int i = 0; i < ec; i++)
            root->children[i] = NULL;

        root->child_count = (short) ec;
    }
    for (int i = 0; i < ec; i++) {

        if (root->children[i] != NULL) {
            continue;
        }
        board_t nb = board_copy(&(root->board));
        nb[legal_moves[i]] = root->player;

        root->children[i] = (Node *) malloc(sizeof(Node));
        root->children[i]->parent = root;
        root->children[i]->player = board_next_player(root->player);
        root->children[i]->visits = 1;
        root->children[i]->value = 0;
        root->children[i]->children = NULL;
        root->children[i]->child_count = 0;
        root->children[i]->board = nb;
    }

    free(legal_moves);
}

player_t mcts_playout_random(Node *root, player_t p) {
    player_t status = board_status(&(root->board));
    if (status == board_next_player(p)) {
        if (root->parent != NULL) {
            root->parent->value = -1;
        }

        return status;
    }

    Node *cn = (Node *) malloc(sizeof(Node));
    memcpy(cn, root, sizeof(Node));
    cn->board = board_copy(&(cn->board));
    while (status == ONGOING) {
        int lm_count = board_empty_count(&(cn->board));
        if (lm_count == 0)
            return status;
        int *legal_moves = board_legal_moves(cn->board);

        int lm = legal_moves[rand() % lm_count];
        free(legal_moves);
        cn->board[lm] = p;

        status = board_status(&(cn->board));
    }

    free(cn->board);
    free(cn);
    return status;
}

void mcts_back_propagate(Node *root, player_t pr) {
    Node *tmp = root;
    while (tmp != NULL) {
        tmp->visits += 1;
        if (tmp->player == board_next_player(pr)) {
            tmp->value += 0.01;
        } else if (tmp->player == pr) {
            tmp->value -= 0.01;
        }

        tmp = tmp->parent;
    }
}

Node *mcts_random_child(Node *root) {
    int cc = root->child_count;
    if (cc == 0)
        return root;

    return root->children[rand() % cc];
}

// =========== PLAYER ============
void player_init(Player **p, player_t symbol) {
    if (*p == NULL)
        *p = (Player *) malloc(sizeof(Player));
    (**p).symbol = symbol;
    (**p).score = 0;
}

void player_destroy(Player *p) {
    free(p);
}

void player_init_all(Player ***p) {
    *p = (Player **) malloc(sizeof(Player *) * 2);
    for (int i = 0; i < 2; i++) {
        (*p)[i] = (Player *) malloc(sizeof(Player));
    }
    player_init(&(*p)[0], PLAYER1);
    player_init(&(*p)[1], PLAYER2);
}

int player_play(Player *p, board_t *board) {
    //time_t start_at = time (NULL);

    unsigned long sim_count = 0;
    Node *r = (Node *) malloc(sizeof(Node));
    r->parent = NULL;
    r->player = p->symbol;
    r->visits = 1;
    r->value = 0;
    r->children = NULL;
    r->child_count = 0;
    r->board = board_copy(board);

    while (sim_count++ < 10000) {
        Node *n = mcts_random_child(r);
        if (board_status(&(n->board)) == ONGOING) {
            mcts_expand(n);
        }

        if (n->child_count > 0) {
            n = mcts_random_child(n);
        }

        player_t pr = mcts_playout_random(n, p->symbol);
        mcts_back_propagate(n, pr);
    }

    Node *best = mcts_best(r);
    long double child_sum = 1;
    /*
    for (int i = 0; i < r->child_count; i++)
      {
        child_sum += mcts_ucb (r->children[i]);
      }
    */

    if (p->symbol == PLAYER1) {
        for (int i = 0; i < BOARD_SIZE; i++) {
            fprintf(f_b_pol, "0,");
            fprintf(f_b_val, "0,");
        }
    } else {
        for (int i = 0; i < BOARD_SIZE; i++) {
            fprintf(f_b_pol, "1,");
            fprintf(f_b_val, "1,");
        }
    }

    long double action_probabilities[9] = {0};
    board_dump_numeric(f_b_pol, &(r->board));
    for (int i = 0; i < r->child_count; i++) {
        action_probabilities[board_diff(*board, r->children[i]->board)] =
                mcts_ucb(r->children[i]) / child_sum;
    }

    for (int i = 0; i < BOARD_SIZE; i++) {
        fprintf(f_b_pol, "%Lf", action_probabilities[i]);
        if (i + 1 < BOARD_SIZE)
            fprintf(f_b_pol, ",");
    }
    fprintf(f_b_pol, "\n");

    board_dump_numeric(f_b_val, &(best->board));
    fprintf(f_b_val, "%Lf\n", best->value);

    int play_to = board_diff(*board, best->board);
    mcts_destroy(r);
    return play_to;
}


//======================================================================

int main(int argc, char **argv) {
    if (argc < 3) {
        printf("USAGE: %s OUTPUT_NAME GAME_COUNT\n", argv[0]);
        exit(0);
    }

    GAME_COUNT = atoi(argv[2]);

    srand(time(NULL));
    printf("MCTS-XOX PQ Generator\n\n%s [%d games]\n\n", argv[1], GAME_COUNT);
    setbuf(stdout, NULL);

    Player **players;
    player_init_all(&players);

    char *f_b_val_name = (char *) malloc(sizeof(char) * 32);
    char *f_b_pol_name = (char *) malloc(sizeof(char) * 32);
    sprintf(f_b_val_name, "./data/%s_val.dat", argv[1]);
    sprintf(f_b_pol_name, "./data/%s_pol.dat", argv[1]);

    mkdir("./data", 0775);
    f_b_val = fopen(f_b_val_name, "w");
    f_b_pol = fopen(f_b_pol_name, "w");
    if (f_b_val == NULL) {
        fprintf(stderr, "Error opening file! (%s)\n", f_b_val_name);
        exit(1);
    }

    if (f_b_pol == NULL) {
        fprintf(stderr, "Error opening file! (%s)\n", f_b_pol_name);
        exit(1);
    }
    for (int _ = 0; _ < GAME_COUNT; _++) {
        printf("Game #%d starts.\n", _);
        board_t board = board_empty();
        // ====== INIT END =======

        char turn_counter = 0;
        while (board_status(&board) == ONGOING) {
            int play = player_play(players[(short) turn_counter], &board);
            if (play < 0) {
                printf("Oops!\n");
                goto cleanup;
            }

            board[play] = players[(short) turn_counter]->symbol;

            turn_counter = (++turn_counter) % 2;
            fprintf(stdout, ".");
        }
        printf("\n");
        switch (board_status(&board)) {
            case PLAYER1:
                players[0]->score += 1;
                break;
            case PLAYER2:
                players[1]->score += 1;
                break;
            default:
                break;
        }
        free(board);
    }

    // ====== CLEANUP =====
    cleanup:
    fclose(f_b_pol);
    fclose(f_b_val);
    for (int i = 0; i < 2; i++)
        player_destroy(players[i]);

    free(players);
    free(f_b_pol_name);
    free(f_b_val_name);
    return 0;
}
