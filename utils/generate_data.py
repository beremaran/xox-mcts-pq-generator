#!/usr/bin/env python

"""
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import glob
import argparse
import subprocess


def merge(output_file, files):
    output_file = open(output_file, 'w')

    print("Merging {} datasets".format(len(files)))
    for bf in files:
        print(bf)
        bfp = open(bf, 'r')
        output_file.writelines(bfp.readlines())
        bfp.close()

    output_file.close()


if __name__ == "__main__":
    arg_parser = argparse.ArgumentParser(prog='generate_data.py',
                                         description='Generates MCTS XOX PQ data with multi-processes')
    arg_parser.add_argument("executable_path")
    arg_parser.add_argument("output_name")
    arg_parser.add_argument("process_count")
    arg_parser.add_argument("game_count")
    args = arg_parser.parse_args()

    p = [
        subprocess.Popen(
            [
                '{} {}_{} {}'.format(
                    args.executable_path, args.output_name, _, int(int(args.game_count) / int(args.process_count))
                )
            ],
            shell=True
        )
        for _ in range(int(args.process_count))
    ]
    exit_codes = [pr.wait() for pr in p]

    pol_files = glob.glob('./data/*_pol.dat')
    val_files = glob.glob('./data/*_val.dat')
    merge('./{}_policy.dat'.format(args.output_name), pol_files)
    merge('./{}_value.dat'.format(args.output_name), val_files)
